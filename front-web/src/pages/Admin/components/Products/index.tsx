import PrivateRoute from "core/components/Routes/PrivateRoute";
import { Route, Switch } from "react-router"
import Form from "./Form"


import List from "./List"

const Products = () => {
    return (
        <div>
            <Switch>
                <Route path="/admin/products" exact>
                    <List />
                </Route>
                <PrivateRoute path="/admin/products/:productId" allowedRoutes={['ROLE_ADMIN']}>
                    <Form />
                </PrivateRoute>
            </Switch>
        </div>
    );
}
export default Products;