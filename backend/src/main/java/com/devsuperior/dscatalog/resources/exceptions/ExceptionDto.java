package com.devsuperior.dscatalog.resources.exceptions;

import java.time.LocalDateTime;


public class ExceptionDto {
	
	private LocalDateTime timestamp;
	
	private String message;
	
	private Integer status;

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ExceptionDto(LocalDateTime timestamp, String message, Integer status) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.status = status;
	}
	public ExceptionDto() {
		
	}
}
