import './styles.scss';
import { ReactComponent as UploadPlaceholder } from 'core/assets/images/upload-placeholder.svg'
import { makePrivateRequest } from 'core/utils/request';
import { useState } from 'react';
import { toast } from 'react-toastify';

type Props = {
    onUploadSucces: (imgUrl:string) => void;
    productImgUrl: string
}

const ImageUpload = ({onUploadSucces,productImgUrl}:Props) => {
    const [uploadProgress, setUploadProgress] = useState(0);
    const [uploadImgUrl, setUploadImgUrl] = useState('');
    const imgUrl = uploadImgUrl || productImgUrl;

    const onUploadProgress = (progressEvent: ProgressEvent) => {
        const progress = Math.round((progressEvent.loaded * 100) / progressEvent.total);
        setUploadProgress(progress);
    }

    const uploadImage = (selectedImage: File) => {
        const payload = new FormData();
        payload.append('file', selectedImage);
        makePrivateRequest({ url: '/products/image', method: 'POST', data: payload, onUploadProgress })
            .then(response => {
                setUploadImgUrl(response.data.uri);
                onUploadSucces(response.data.uri);
            })
            .catch(() => {
                toast.error('Erro ao enviar Arquivo!');
            })
            .finally(() => {
                setUploadProgress(0)
            })
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const selectedImage = event.target.files?.[0];
        if (selectedImage) {
            uploadImage(selectedImage);
        }
    }

    return (
        <div className="row">
            <div className="col-6">
                <div className="upload-button-container">
                    <input type="file" id="upload" accept="image/png, image/jpg" onChange={handleChange} hidden />
                    <label htmlFor="upload" className="label-input">ADICIONAR IMAGEM</label>
                </div>
                <small className="upload-test-helper text-primary">
                    As imagens devem ser JPG ou PNG e não devem ultrapassar <strong> 5mb.</strong>
                </small>
            </div>
            <div className="upload-placeholder">
                {uploadProgress > 0 && (
                    <>
                        <UploadPlaceholder />
                        <div className="upload-progress-container">
                            <div className="upload-progress" style={{ width: `${uploadProgress}%` }}>

                            </div>
                        </div>
                    </>
                )}
                {(imgUrl && uploadProgress === 0 )&& (
                    <img className="uploaded-image" src={imgUrl} alt={imgUrl}/>
                )}
            </div>

        </div>
    );
}

export default ImageUpload;