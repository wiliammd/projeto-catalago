import Pagination from "core/components/Pagination";
import { CategoriesResponse, ErroDto } from "core/types/Product";
import { makePrivateRequest, makeRequest } from "core/utils/request";
import LoaderAdminProduct from "pages/Catalog/components/Loaders/LoaderAdminProduct";
import { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import Card from "../../Products/Card";

const ListSub = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [activePage, setActivePage] = useState(0);
    const history = useHistory();
    const [categoriesResponse, setCategoriesResponse] = useState<CategoriesResponse>();
    const [name, setName] = useState('');



    const getGruop = useCallback(() => {
        const params = {
            page: activePage,
            linesPerPage: 8,

        }
        setIsLoading(true);
        makePrivateRequest({ url: `/subcategorie/`, params, method: 'GET' })
            .then(response => setCategoriesResponse(response.data))
            .catch(() => toast.error("Erro ao carregar lista!"))
            .finally(() => { setIsLoading(false) })
    }, [activePage])

    useEffect(() => {
        getGruop();
    }, [getGruop])


    //isso aqui vai servir para quando for necessario realizar uma busca por nome
    // const handleChangeName = (name: string) => {
    //     setName(name);

    // }
    const handleExcluir = (excluir: number) => {
        const confirm = window.confirm('Deseja Realmente excluir esta subCat?');
        if (confirm) {
            makePrivateRequest({ url: `/subcategorie/${excluir}`, method: 'DELETE' })
                .then(() => {
                    toast.info(`subcategorie de ID:${excluir} excluido com sucesso!`);
                    getGruop();
                }
                )
                .catch(() => toast.error("Erro ao excluir subcategorie!"))
        }
    }
    const handleCreate = () => {
        history.push('/admin/subcategories/create');
    }

    return (
        <div>

            <div className="admin-list-container">
                <div className="container">
                    <div className="meu-base-form p-2 m-5 table-responsive">
                        <div className="text-end">
                            <button className="btn btn-primary btn-lg mr-1" onClick={handleCreate}>
                                Adicionar
                            </button>
                        </div>
                        <table className="table table-hover">
                            <thead >
                                <tr>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Nome da SubCategoria</th>
                                    <th scope="col">Ações</th>

                                </tr>
                            </thead>
                            <tbody>
                                {isLoading ? <tr key={"22"}><td ></td></tr> : categoriesResponse?.content.map(projeto => (
                                    <>
                                        <tr key={projeto.id} >
                                            <td>{projeto.id}</td>
                                            <td>{projeto.name}</td>

                                            <td ><div className="d-flex"> <div className="ms-3" onClick={() => handleExcluir(projeto.id)}><i className="fa fa-trash text-danger fa-2x"></i></div></div></td>
                                        </tr>

                                    </>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                {categoriesResponse &&
                    <Pagination
                        totalPages={categoriesResponse.totalPages}
                        activePage={activePage}
                        onChange={page => setActivePage(page)}
                    />}
            </div>
        </div>
    );
}
export default ListSub;