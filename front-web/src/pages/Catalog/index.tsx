import ProductCard from './components/ProductCard';
import { Link } from 'react-router-dom';

import './styles.scss'
import { useCallback, useEffect, useState } from 'react';
import { makeRequest } from 'core/utils/request';
import { Category, ProductsResponse } from 'core/types/Product';
import ProductCardLoader from './components/Loaders/ProductCardLoader';
import Pagination from 'core/components/Pagination';
import ProductFilters from 'core/components/ProductFilters';

const Catalog = () => {
    //quando a lista de produtos estiver disponivel, popular um estado no componente e listar os produtos dinamicamente
    const [productsResponse, setProductsResponse] = useState<ProductsResponse>();
    const [isLoading, setIsLoading] = useState(false);
    const [activePage, setActivePage] = useState(0);
    const [name, setName] = useState('');
    const [category, setCategory] = useState<Category>();


    const getProducts = useCallback(() => {
        const params = {
            page: activePage,
            linesPerPage: 12,
            name: name,
            categoryId: category?.id
        }
        setIsLoading(true);
        makeRequest({ url: '/products', params }).then(response => setProductsResponse(response.data))
            .finally(() => {
                setIsLoading(false);
            })
    }, [activePage,name,category]);


    //quando o componente iniciar, buscar os dados de produtos
    useEffect(() => {
        getProducts();
    }, [getProducts]);

    const handleChangeName = (name: string) => {
        setActivePage(0);
        setName(name);
       
    }
    const handleChangeCategory = (category: Category) => {
        setCategory(category);
       
    }
    const clearFilters = () =>{
        setCategory(undefined);
        setName('');
        
    }

    //console.log(productsResponse)

    return (
        <div className="catalog-container">
            <div className="filter-container">
                <h1 className="catalog-title">Catálogo de produtos</h1>
                <ProductFilters name={name} category={category} handleChangeCategory={handleChangeCategory} 
                handleChangeName={handleChangeName} clearFilters={clearFilters} />
            </div>
            <div className="catalog-produts">
            {isLoading ? <ProductCardLoader /> : (
                    productsResponse?.content.map(product => (
                        
                        <Link to={`/products/${product.id}`} key={product.id}>
                            <ProductCard product={product} />
                        </Link>
                    ))
                )}

            </div>
            {productsResponse &&
                <Pagination
                    totalPages={productsResponse.totalPages}
                    activePage={activePage}
                    onChange={page => setActivePage(page)}
                />}
        </div>

    );
}
export default Catalog;