import { makePrivateRequest } from "core/utils/request";
import Pagination from 'core/components/Pagination';
import { useCallback, useEffect, useState } from "react";
import { OrderBy, UsersResponse } from 'core/types/Product';
import LoaderAdminProduct from 'pages/Catalog/components/Loaders/LoaderAdminProduct';
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import Card from "../Card";
import UsersFilters from "core/components/UsersFilters";

const List = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [activePage, setActivePage] = useState(0);
    const history = useHistory();
    const [usersResponse, setUsersResponse] = useState<UsersResponse>();
    const [name, setName] = useState('');
    const [orderBy, setOrderBy] = useState<OrderBy>();


    const getUsers = useCallback(() => {
        const params = {
            page: activePage,
            linesPerPage: 4,
            direction: orderBy?.direction,
            orderBy: orderBy?.orderBy,
            name,
            orderById: orderBy?.id
        }
        //iniciar o loader
        setIsLoading(true);
        makePrivateRequest({ url: '/users', params })
            .then(response => setUsersResponse(response.data))
        
            .finally(() => {
                //finalizar o loader
                setIsLoading(false);
            })
    }, [activePage,name,orderBy]);
    //console.log(usersResponse?.content);
    //console.log(productsResponse);
    //quando o componente iniciar, buscar os dados de produtos
    useEffect(() => {
        getUsers();
    }, [getUsers]);

    const handleCreate = () => {
        history.push('/admin/users/create');
    }
    const handleChangeName = (name: string) => {
        setActivePage(0);
        setName(name);
       
    }
    const clearFilters = () =>{
        setName('');
        setOrderBy(undefined);
        
    }
    const handleChangeOrderBy = (ordenacao: OrderBy) => {
        setOrderBy(ordenacao);
       
    }

    const onRemove = (userId: number) => {
        const confirm = window.confirm('Deseja Realmente excluir esta Usuario?');
        if (confirm) {
            makePrivateRequest({ url: `/users/${userId}`, method: 'DELETE' })
                .then(() => {
                    toast.info(`Usuario de ID:${userId} excluido com sucesso!`);
                    getUsers();
                }
                )
                .catch(() => toast.error("Erro ao excluir Usuario!"))
        }

    }

    return (
        <div>
            <div className="d-flex justify-content-between">
                <button className="btn btn-primary btn-lg mr-1" onClick={handleCreate}>
                    Adicionar
                </button>
                <UsersFilters  name={name}
                    handleChangeName={handleChangeName}
                    clearFilters={clearFilters}
                    handleChangeOrderBy={handleChangeOrderBy}
                    orderBy={orderBy}
                />
            </div>
            <div className="admin-list-container">
                {isLoading ? <LoaderAdminProduct /> : usersResponse?.content.map(user => (
                    <Card user={user} key={user.id} onRemove={onRemove} />
                ))}
                {usersResponse &&
                    <Pagination
                        totalPages={usersResponse.totalPages}
                        activePage={activePage}
                        onChange={page => setActivePage(page)}
                    />}
            </div>
        </div>
    );
}
export default List;