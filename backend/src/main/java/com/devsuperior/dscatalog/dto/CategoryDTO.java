package com.devsuperior.dscatalog.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.entities.SubCategorie;

public class CategoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private List<SubCategorie> subCategories = new ArrayList<>();

	public List<SubCategorie> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<SubCategorie> subCategories) {
		this.subCategories = subCategories;
	}

	public CategoryDTO() {

	}

	public CategoryDTO(Category entity) {

		this.id = entity.getId();
		this.name = entity.getName();
		this.subCategories = entity.getSubCategories().stream().collect(Collectors.toList());
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
