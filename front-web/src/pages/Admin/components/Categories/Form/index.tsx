import { toast } from 'react-toastify';
import { makePrivateRequest, makeRequest } from 'core/utils/request';
import { useForm } from 'react-hook-form';
import BaseForm from '../../BaseForm';
import './styles.scss';
import { useHistory, useParams } from 'react-router';
import { useEffect, useState } from 'react';
import { SubCategories } from 'core/types/Product';


export type FormState = {
    name: string;
    subCategories: string[];
}
type ParamsType = {
    categoryId: string;

}

const Form = () => {
    const { register, handleSubmit, errors, setValue } = useForm<FormState>();
    const history = useHistory();
    const { categoryId } = useParams<ParamsType>();
    const [subCategories, setSubCategories] = useState<SubCategories[]>();
    const isEditing = categoryId !== 'create';
    const formTitle = isEditing ? "Editar Categoria" : "Categoria um Produto";

    useEffect(() => {
        if (isEditing) {
            makeRequest({ url: `/categories/${categoryId}` })
                .then(response => {
                    setValue('name', response.data.name);
                    setValue('subCategories', response.data.subCategories);
                    setSubCategories(response.data.subCategories);
                    console.log(subCategories);
                });
        }
    }, [categoryId, isEditing, setValue]);


    const onSubmit = (data: FormState) => {
        const payload = {
            ...data
        }
        makePrivateRequest({ url: isEditing ? `/categories/${categoryId}` : '/categories', method: isEditing ? 'PUT' : 'POST', data: payload })
            .then(() => {
                toast.info(isEditing ? 'Categoria Alterado com sucesso!' : 'Categoria cadastrado com sucesso!');
                history.push('/admin/categories');
            })
            .catch(() => {
                toast.error('Erro ao salvar Categoria!');
            })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <BaseForm title={formTitle}>
                <div className="row">
                    <div className="col-6">
                        <div className="margin-bottom-30">
                            <input name="name" type="text" placeholder="nome"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>
                            {errors.name && (
                                <div className="invalid-feedback d-block">
                                    {errors.name.message}
                                </div>)}
                            <table className="table table-hover">
                            <thead >
                            <tr>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nome SubCategoria</th>
                          

                            </tr>
                            </thead>
                            <tbody>
                        {subCategories?subCategories.map(subCat => (
                             <>
                             <tr key={subCat.id} >
                                 <td>{subCat.id}</td>
                                 <td>{subCat.name}</td>
                                
                             </tr>

                         </>
                        )): ''}
                         </tbody>
                    </table>

                        </div>

                    </div>
                </div>
            </BaseForm>
        </form>
    );
}
export default Form;