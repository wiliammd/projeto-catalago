package com.devsuperior.dscatalog.dto;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.devsuperior.dscatalog.entities.Category;

public class CategorySelectDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;


	public CategorySelectDTO() {

	}

	public CategorySelectDTO(Category entity) {

		this.id = entity.getId();
		this.name = entity.getName();
		
	}
	public static List<CategorySelectDTO> converter(List<Category> categories) {
		return categories.stream().map(CategorySelectDTO::new).collect(Collectors.toList());
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
