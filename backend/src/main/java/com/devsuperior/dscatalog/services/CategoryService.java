package com.devsuperior.dscatalog.services;


import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devsuperior.dscatalog.dto.CategoryDTO;
import com.devsuperior.dscatalog.dto.CategorySelectDTO;
import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.repositories.CategoryRepository;
import com.devsuperior.dscatalog.repositories.SubCategorieRepository;
import com.devsuperior.dscatalog.services.execptions.DatabaseException;
import com.devsuperior.dscatalog.services.execptions.ResourceNotFoundException;


@Service
public class CategoryService {
	@Autowired
	private CategoryRepository repository;
	@Autowired SubCategorieRepository subCatRepository;
	
	@Transactional(readOnly = true)
	public Page<CategoryDTO> findAllPaged(String name, PageRequest pageRequest){
		Page<Category>list =  repository.findByNameContainingIgnoreCase(name, pageRequest);
		return list.map(x -> new CategoryDTO(x));
	}
	@Transactional(readOnly = true)
	public CategoryDTO findById(Long id) {
		Optional<Category> obj = repository.findById(id);
		Category entity = obj.orElseThrow(()-> new ResourceNotFoundException("Entity Not Found! - Entidade não encontrada!"));
		return new CategoryDTO(entity);
	}
	
	@Transactional
	public CategoryDTO insert(CategoryDTO dto) {
		Category entity = new Category();
		entity.setName(dto.getName());
		entity = repository.save(entity);
		return new CategoryDTO(entity);
	}
	public ResponseEntity<List<CategorySelectDTO>> listarCatSelect() {
		List<Category> cat = this.repository.findAll();
		return ResponseEntity.ok(CategorySelectDTO.converter(cat));

	}
	@Transactional
	public CategoryDTO update(Long id, CategoryDTO dto) {
		try {
		Category entity = repository.getOne(id);
		entity.setName(dto.getName());
		entity = repository.save(entity);
		return new CategoryDTO(entity);
		}catch(EntityNotFoundException e) {
			throw new ResourceNotFoundException("Id not found - Id não encontrado!" +id);
		}
		
	}
	public void  delete(Long id) {
		try {
			repository.deleteById(id);
			
			
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Id not found - Id não encontrado!" +id);
		}
		catch(DataIntegrityViolationException e) {
			throw new DatabaseException("Integrity violation - Violação de integridade");
		}
	}

}
