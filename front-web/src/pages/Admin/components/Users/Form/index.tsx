import { toast } from 'react-toastify';
import { makePrivateRequest } from 'core/utils/request';
import { useForm, Controller} from 'react-hook-form';
import Select from 'react-select';
import BaseForm from '../../BaseForm';
//import './styles.scss';
import { useHistory, useParams } from 'react-router';
import { useEffect } from 'react';
import { Role, RoleForm } from 'core/types/Product';
import { useState } from 'react';


export type FormState = {
    email: string;
    firstName: string;
    lastName: string;
    roles: Role[];

}
type ParamsType = {
    userId: string;
}

const Form = () => {
    const { register, handleSubmit, errors, setValue, control } = useForm<FormState>();

    const history = useHistory();
    const [valid, setValid] = useState(false);
    const { userId } = useParams<ParamsType>();
    const [roless] = useState<RoleForm[]>([
        {id:1, authority:'ROLE_OPERATOR',name:'Operador'},
        {id:2, authority:'ROLE_ADMIN',name:'Admininstrador'}
    ]);
    const [password, setPassword] = useState({
        password: '',
        secondPassword: ''
    })
    const inputChange: (event: React.ChangeEvent<HTMLInputElement>) => void = (event) => {
        const { value, name } = event.target;
        setPassword({
            ...password,
            [name]: value
        })
    }


    const isEditing = userId !== 'create';
    const formTitle = isEditing ? "Editar Usuario" : "Criar um Usuario";
    
    useEffect(() => {
        setValid(password.password === password.secondPassword)
    }, [password])
    useEffect(() => {
        if (isEditing) {
            makePrivateRequest({ url: `/users/${userId}` })
                .then(response => {
                    setValue('email', response.data.email);
                    setValue('firstName', response.data.firstName);
                    setValue('lastName', response.data.lastName);
                    setValue('roles', response.data.roles);
                });
        }
    }, [userId, isEditing, setValue]);
    

    const onSubmit = (data: FormState) => {
        const payload = {
            ...data
        }
        if (valid) {
            makePrivateRequest({ url: isEditing ? `/users/${userId}` : '/users', method: isEditing ? 'PUT' : 'POST', data: payload })
                .then(() => {
                    toast.info(isEditing ? 'Usuário Alterado com sucesso!' : 'Usuário cadastrado com sucesso!');
                    history.push('/admin/users');
                })
                .catch(() => {
                    toast.error('Erro ao salvar Usuário!');
                })
        }
        else {
            toast.error('Senhas não compativeis');
        }
    }


    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <BaseForm title={formTitle}>
                <div className="row">
                    <div className="col-6">
                        <div className="margin-bottom-30">
                            <input name="email" type="text" placeholder="Email"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>
                            {errors.email && (
                                <div className="invalid-feedback d-block">
                                    {errors.email?.message}
                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            <input name="firstName" type="text" placeholder="Nome"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>

                            {errors.email && (
                                <div className="invalid-feedback d-block">
                                    {errors.email?.message}

                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            <input name="lastName" type="text" placeholder="Sobrenome"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>
                            {errors.email && (
                                <div className="invalid-feedback d-block">
                                    {errors.email?.message}
                                </div>)}
                        </div>
                        {/* aqui */}
                        <div className="mb-4">

                            <Controller defaultValue=""
                                name="roles" rules={{ required: true }} control={control} 
                                as={Select} options={roless} getOptionLabel={(option: RoleForm) => option.authority}
                                getOptionValue={(option: RoleForm) => String(option.id)}
                                placeholder="Perfil de Usuário" isMulti classNamePrefix="categories-select" />
                            {errors.roles && (
                                <div className="invalid-feedback d-block">
                                    Campo obrigatório!!
                                </div>)}

                        </div>
                        {/* aqui */}
                        {isEditing ? '' : <><div className="margin-bottom-30">
                            <input name="password" type="password" placeholder="senha"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório",  minLength:{value:5, message:"Senha deve ter mais que 5 caracteres"} })} onChange={inputChange}></input>
                            {errors.email && (
                                <div className="invalid-feedback d-block">
                                    {errors.email?.message}
                                </div>)}
                        </div>
                            <div className="margin-bottom-30">
                                <input name="secondPassword" type="password" placeholder="repita a senha"
                                    className="form-control input-base" ref={register({ required: "Campo obrigatório" })} onChange={inputChange}></input>
                                <div className="invalid-feedback d-block">
                                    {valid ? '' : 'As senhas não são iguais'}
                                </div>
                                {errors.email && (
                                    <div className="invalid-feedback d-block">
                                        {errors.email?.message}
                                    </div>)}
                            </div> </>}


                    </div>
                </div>
            </BaseForm>
        </form>
    );
}
export default Form;