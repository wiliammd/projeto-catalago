package com.devsuperior.dscatalog.resources;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.devsuperior.dscatalog.dto.SubCategorieDTO;
import com.devsuperior.dscatalog.entities.SubCategorie;
import com.devsuperior.dscatalog.services.SubCategorieService;
@RestController
@RequestMapping(value = "/subcategorie")
public class SubCatagorieResource {
	@Autowired
	private SubCategorieService subCatService;
	@GetMapping
	public ResponseEntity<Page<SubCategorie>> findAll(
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "12") Integer linesPerPage,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "orderBy", defaultValue = "name") String orderBy
			
			
			){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage,Direction.valueOf(direction), orderBy);
				
		Page<SubCategorie> list = subCatService.findAllPaged( pageRequest);
		return ResponseEntity.ok().body(list);
	}
	@DeleteMapping(value = "/{id}")
	public ResponseEntity <SubCategorie> delete(@PathVariable Long id){
		subCatService.delete(id);
		return ResponseEntity.noContent().build();
	}
	@PostMapping
	public ResponseEntity<SubCategorieDTO> insert(@RequestBody SubCategorieDTO form){
		form = subCatService.insert(form);
		//URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(form.getId()).toUri();
		return ResponseEntity.ok().body(form);		
	}

}
