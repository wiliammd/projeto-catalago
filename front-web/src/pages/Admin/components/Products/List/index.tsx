import Pagination from 'core/components/Pagination';
import { Category, ProductsResponse } from 'core/types/Product';
import { makePrivateRequest, makeRequest } from 'core/utils/request';
import LoaderAdminProduct from 'pages/Catalog/components/Loaders/LoaderAdminProduct';
import { useEffect, useState, useCallback } from 'react';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import Card from '../Card';
import ProductFilters from 'core/components/ProductFilters';


const List = () => {
    //quando a lista de produtos estiver disponivel, popular um estado no componente e listar os produtos dinamicamente
    const [productsResponse, setProductsResponse] = useState<ProductsResponse>();
    const [isLoading, setIsLoading] = useState(false);
    const [activePage, setActivePage] = useState(0);
    const history = useHistory();
    const [name, setName] = useState('');
    const [category, setCategory] = useState<Category>();


    const getProducts = useCallback(() => {
        const params = {
            page: activePage,
            linesPerPage: 4,
            direction: 'DESC',
            orderBy: 'id',
            name: name,
            categoryId: category?.id
        }
        //iniciar o loader
        setIsLoading(true);
        makeRequest({ url: '/products', params }).then(response => setProductsResponse(response.data))
            .finally(() => {
                //finalizar o loader
                setIsLoading(false);
            })
    }, [activePage,name,category]);

    //teste de criacao
    // const atualizaProducts = useCallback(() => {
    //     //productsResponse?.content
    // }, [category]);

    //quando o componente iniciar, buscar os dados de produtos
    useEffect(() => {
        getProducts();
    }, [getProducts]);

    const onRemove = (productId: number) => {
        const confirm = window.confirm('Deseja Realmente excluir este produto?');
        if (confirm) {
            makePrivateRequest({ url: `/products/${productId}`, method: 'DELETE' })
                .then(() => {
                    toast.info(`Produto de ID:${productId} excluido com sucesso!`);
                    getProducts();
                }
                )
                .catch(() => toast.error("Erro ao excluir Produto!"))
        }

    }

    const handleCreate = () => {
        history.push('/admin/products/create');
    }
    const handleChangeName = (name: string) => {
        setActivePage(0);
        setName(name);
       
    }
    const handleChangeCategory = (category: Category) => {
        setCategory(category);
       
    }
    const clearFilters = () =>{
        setCategory(undefined);
        setName('');
        
    }

    return (
        <div className="admin-products-list">
            <div className="d-flex justify-content-between">
                <button className="btn btn-primary btn-lg mr-1" onClick={handleCreate}>
                    Adicionar
                </button>
                <ProductFilters name={name} category={category} handleChangeCategory={handleChangeCategory} 
                handleChangeName={handleChangeName} clearFilters={clearFilters} />

            </div>
            <div className="admin-list-container">
                {isLoading ? <LoaderAdminProduct /> : productsResponse?.content.map(product => (
                    <Card product={product} key={product.id} onRemove={onRemove} />
                ))}
                {productsResponse &&
                    <Pagination
                        totalPages={productsResponse.totalPages}
                        activePage={activePage}
                        onChange={page => setActivePage(page)}
                    />}
            </div>
        </div>

    );
}

export default List;