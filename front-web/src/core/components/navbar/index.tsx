import './styles.scss';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { getAccessTokenDecoded, logout } from 'core/utils/auth';
import { useEffect, useState } from 'react';


const Navbar = () => {
    const [currentUser, setCurrentUser] = useState('');
    const location = useLocation();

    useEffect(() => {
        //console.log('navbar iniciada! ',location);
        const currentUserData = getAccessTokenDecoded();
        setCurrentUser(currentUserData.user_name);
    }, [location]);
    const [drawerActive, setDrawerActive] = useState(false);

    const handleLogout = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        logout();
    }

    return (
        (
            <nav className="bg-primary main-nav">

                <Link to="/" className="nav-logo-text" onClick={() => setDrawerActive(false)}>
                    <h4>Ds Catalog</h4>
                </Link>

                <button className="menu-mobile-btn" type="button" onClick={() => setDrawerActive(!drawerActive)}><i className="text-light fas fa-bars"></i></button>


                <div className={drawerActive ? "menu-mobile-container" : "menu-container"}>
                    <ul className="main-menu">
                        <li>
                            <NavLink className="nav-link" to="/" exact onClick={() => setDrawerActive(false)}>HOME</NavLink>
                        </li>
                        <li>
                            <NavLink className="nav-link" to="/products" onClick={() => setDrawerActive(false)}>CATÁLOGO</NavLink>
                        </li>

                        <li>
                            <NavLink className="nav-link" to="/admin" onClick={() => setDrawerActive(false)}>ADMIN</NavLink>
                        </li>
                      
                        {
                            drawerActive && (
                                <>

                                    {!currentUser && (
                                        <li>
                                            <Link className="nav-link active" to="/auth/login" onClick={() => setDrawerActive(false)}>Login</Link>
                                        </li>
                                    )}

                                    {currentUser && (
                                        <li>
                                            <Link className="nav-link active" to="/home" onClick={(e) => {setDrawerActive(false); handleLogout(e); }}>{`LogOut - ${currentUser}`}</Link>
                                        </li>
                                    )}
                                </>
                            )
                        }

                    </ul>

                </div>
                <div className="user-info-dnone text-right ">
                    {currentUser && (
                        <>
                            {currentUser}
                            <a href="/home" className="nav-link active d-inline" onClick={(e) => { setDrawerActive(false); handleLogout(e); }}>
                                LogOut
                            </a>
                        </>
                    )}
                    {!currentUser && (

                        <Link className="nav-link active" to="/auth/login" onClick={() => setDrawerActive(false)}>LogIn</Link>

                    )}
                </div>
            </nav>
        )
    );
}

export default Navbar;