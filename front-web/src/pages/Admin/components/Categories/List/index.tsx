import { makePrivateRequest, makeRequest } from "core/utils/request";
import Pagination from 'core/components/Pagination';
import { useCallback, useEffect, useState } from "react";
import { CategoriesResponse, ErroDto, OrderBy } from 'core/types/Product';
import LoaderAdminProduct from 'pages/Catalog/components/Loaders/LoaderAdminProduct';
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import Card from "../Card";
import SearchFilters from "core/components/SearchFilters";

const List = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [activePage, setActivePage] = useState(0);
    const history = useHistory();
    const [categoriesResponse, setCategoriesResponse] = useState<CategoriesResponse>();
    const [name, setName] = useState('');
    const [orderBy, setOrderBy] = useState<OrderBy>();


    const getCategories = useCallback(() => {
        const params = {
            page: activePage,
            linesPerPage: 4,
            direction: orderBy?.direction,
            orderBy: orderBy?.orderBy,
            name,
            orderById: orderBy?.id
        }
        //iniciar o loader
        setIsLoading(true);
        makeRequest({ url: '/categories', params }).then(response => setCategoriesResponse(response.data))
            .finally(() => {
                //finalizar o loader
                setIsLoading(false);
            })
    }, [activePage,name,orderBy]);
    //console.log(categoriesResponse?.content);
    //console.log(productsResponse);
    //quando o componente iniciar, buscar os dados de produtos
    useEffect(() => {
        getCategories();
    }, [getCategories]);

    const handleCreate = () => {
        history.push('/admin/categories/create');
    }
    const handleChangeName = (name: string) => {
        setActivePage(0);
        setName(name);
       
    }
    const clearFilters = () =>{
        setName('');
        setOrderBy(undefined);
        
    }
    const handleChangeOrderBy = (ordenacao: OrderBy) => {
        setOrderBy(ordenacao);
       
    }

    const onRemove = (categoryId: number) => {
        const confirm = window.confirm('Deseja Realmente excluir esta categoria?');
        if (confirm) {
            makePrivateRequest({ url: `/categories/${categoryId}`, method: 'DELETE' })
                .then(() => {
                    toast.info(`Categoria de ID:${categoryId} excluido com sucesso!`);
                    getCategories();
                }
                )
                .catch((e) => {let erro = e.response.data as ErroDto;toast.error("Erro ao excluir Categoria!"+erro.message)})
        }

    }

    return (
        <div>
            <div className="d-flex justify-content-between">
                <button className="btn btn-primary btn-lg mr-1" onClick={handleCreate}>
                    Adicionar
                </button>
                <SearchFilters  name={name}
                    handleChangeName={handleChangeName}
                    clearFilters={clearFilters}
                    handleChangeOrderBy={handleChangeOrderBy}
                    orderBy={orderBy}
                />
            </div>
            <div className="admin-list-container">
                {isLoading ? <LoaderAdminProduct /> : categoriesResponse?.content.map(category => (
                    <Card category={category} key={category.id} onRemove={onRemove} />
                ))}
                {categoriesResponse &&
                    <Pagination
                        totalPages={categoriesResponse.totalPages}
                        activePage={activePage}
                        onChange={page => setActivePage(page)}
                    />}
            </div>
        </div>
    );
}
export default List;