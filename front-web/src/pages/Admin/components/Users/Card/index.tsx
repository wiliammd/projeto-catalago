
import { User } from 'core/types/Product';
import { Link } from 'react-router-dom';
import './styles.scss';

type Props = {
    user: User;
    onRemove: (usersId: number) => void;
}

const Card = ({ user, onRemove }: Props) => {
    return (
        <div className="card-base user-card-admin" >
            <div className="row">
                <div className="col-4 text-center  py-3">
                    <h3 className="user-card-name-admin">{user.email}</h3>
                </div>
                <div className="col-4 py-3">
                    <div className="row">
                        <h3 className="user-card-name-admin">{user.firstName}</h3>
                        <h3 className="user-card-name-admin2">{user.lastName}</h3>
                    </div>

                    <div>
                        {user.roles.map(role => (

                            <span className="badge badge-secondary mr-2" key={role.id}>{role.authority}</span>



                        ))}
                    </div>

                </div>
                <div className="col-4 pt-3 pr-5 ">
                    <Link to={`/admin/users/${user.id}`} type="button" className="btn btn-outline-secondary btn-block border-radius-10 mb-3">Editar</Link>
                    <button type="button" onClick={() => onRemove(user.id)} className="btn btn-outline-danger btn-block border-radius-10 mb-3">Excluir</button>
                </div>
            </div>
        </div>
    )
}
export default Card;