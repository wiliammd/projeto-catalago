package com.devsuperior.dscatalog.dto;

import java.io.Serializable;

import com.devsuperior.dscatalog.entities.SubCategorie;

public class SubCategorieDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Long category;
	public SubCategorieDTO(SubCategorie entity) {
		
		this.id = entity.getId();
		this.name = entity.getName();
		this.category = entity.getCategory().getId() != null? entity.getCategory().getId(): null;
	}
	public SubCategorieDTO() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCategory() {
		return category;
	}
	public void setCategory(Long category) {
		this.category = category;
	}
}
