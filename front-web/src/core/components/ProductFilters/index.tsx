import './styles.scss';
import { ReactComponent as SearchIcon } from 'core/assets/images/search-icon.svg';
import Select from 'react-select';
import { useEffect, useState } from 'react';
import { makeRequest } from 'core/utils/request';
import { Category } from 'core/types/Product';


type Props = {
    name?: string;
    category?: Category;
    handleChangeName: (value: string) => void;
    handleChangeCategory: (category: Category) => void;
    clearFilters: () => void;
}


const ProductFilters = ({ name, handleChangeName, handleChangeCategory, clearFilters, category }: Props) => {
    const [isLoadingCategories, setIsloadingCategories] = useState(false);
    const [categories, setCategories] = useState<Category[]>([]);

    // console.log("aqui2 "+name);
    //quando o componente iniciar, buscar os dados de produtos
    useEffect(() => {
        setIsloadingCategories(true);
        makeRequest({ url: '/categories' }).then(response => setCategories(response.data.content))
            .finally(() => {
                //finalizar o loader
                setIsloadingCategories(false);
            })
    }, []);


    return (
        <div className="card-base product-filters-container">
            <div className="input-search ">
                <input type="text" value={name} className="form-control" placeholder="Pesquisar Produto"
                    onChange={event => handleChangeName(event.target.value)} />
                <SearchIcon />
            </div>
            <Select className="filter-select-container" isClearable value={category} 
            key={`select-${category?.id}`}
                name="categories" isLoading={isLoadingCategories}
                 onChange={value => handleChangeCategory(value as Category)}
                options={categories} getOptionLabel={(option: Category) => option.name}
                getOptionValue={(option: Category) => String(option.id)}
                placeholder="Categoria" classNamePrefix="product-categories-select" />
            <button className="btn btn-outline-secondary border-radius-10" onClick={clearFilters}>Limpar Filtro</button>
        </div>
    )
}
export default ProductFilters;