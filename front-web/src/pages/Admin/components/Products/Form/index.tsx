import { toast } from 'react-toastify';
import { makePrivateRequest, makeRequest } from 'core/utils/request';
import { useForm, Controller} from 'react-hook-form';
import BaseForm from '../../BaseForm';
import './styles.scss';
import { useHistory, useParams } from 'react-router';
import { useEffect, useState } from 'react';
import Select from 'react-select';
import { Category } from 'core/types/Product';
import PriceField from './PriceField';
import ImageUpload from '../ImageUpload';

export type FormState = {
    name: string;
    price: string;
    categories:Category[];
    description: string;
    imgUrl: string;
}
type ParamsType = {
    productId: string;
}
// const options = [
//     { value: 'livros', label: 'Livros' },
//     { value: 'eletronicos', label: 'Eletrônicos' },
//     { value: 'computadores', label: 'Computadores' }
// ]

const Form = () => {
    const { register, handleSubmit, errors, setValue, control} = useForm<FormState>();
    const history = useHistory();
    const [isLoadingCategories, setIsloadingCategories] = useState(false);
    const [categories, setCategories] = useState<Category[]>([]);
    const { productId } = useParams<ParamsType>();
    const isEditing = productId !== 'create';
    const formTitle = isEditing ? "Editar Produto" : "Cadastrar um Produto";
    const [uploadImgUrl, setUploadImgUrl] = useState(''); 
    const [productImgUrl, setProductImgUrl] = useState(''); 
    // console.log(isEditing);
    // console.log(isEditing);
    useEffect(() => {
        if (isEditing) {
            makeRequest({ url: `/products/${productId}` })
                .then(response => {
                    setValue('name', response.data.name);
                    setValue('price', response.data.price);
                    setValue('description', response.data.description);
                    
                    setValue('categories',response.data.categories);
                    setProductImgUrl(response.data.imgUrl);
                });
        }
    }, [productId, isEditing, setValue]);
    
    useEffect(() => {
        setIsloadingCategories(true);
        makeRequest({ url: '/categories' })
            .then(response => setCategories(response.data.content))
            .finally((() => setIsloadingCategories(false)));
    }, []);

    const onSubmit = (data: FormState) => {
        const payload = {
            ...data,
            imgUrl: uploadImgUrl || productImgUrl
        }
        makePrivateRequest({ url: isEditing ? `/products/${productId}` : '/products', method: isEditing ? 'PUT' : 'POST', data: payload })
            .then(() => {
                toast.info(isEditing ? 'Produto Alterado com sucesso!' : 'Produto cadastrado com sucesso!');
                history.push('/admin/products');
            })
            .catch(() => {
                toast.error('Erro ao salvar produto!');
            })
    }
    const onUploadSuccess = (imgUrl: string) =>{
        setUploadImgUrl(imgUrl);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <BaseForm title={formTitle}>
                <div className="product-form-container">
                    <div className="col-6">
                        <div className="margin-bottom-30">
                            <input name="name" type="text" placeholder="nome"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>
                            {errors.name && (
                                <div className="invalid-feedback d-block">
                                    {errors.name.message}
                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            <Controller defaultValue=""
                            name="categories" rules={{required:true}} control={control} isLoading={isLoadingCategories}
                            as={Select} options={categories} getOptionLabel={(option: Category) => option.name}
                                getOptionValue={(option: Category) => String(option.id)} 
                                placeholder="Categoria" isMulti classNamePrefix="categories-select" />
                                {errors.categories && (
                                <div className="invalid-feedback d-block">
                                    Campo obrigatório!!
                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            
                            <PriceField control={control}/>
                                {errors.price && (
                                <div className="invalid-feedback d-block">
                                    {errors.price.message}
                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            <ImageUpload onUploadSucces={onUploadSuccess} productImgUrl={productImgUrl}/>
                            {/* <input name="imgUrl" type="text" placeholder="imgUrl" className="form-control input-base" ref={register({ required: "Campo obrigatório" })} /> */}
                            {errors.imgUrl && (
                                <div className="invalid-feedback d-block">
                                    {errors.imgUrl.message}
                                </div>)}
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="margin-bottom-30">
                            <textarea className="form-control input-base" placeholder="Descrição" name="description" ref={register({ required: "Campo obrigatório" })} id="" cols={30} rows={10} />
                            {errors.description && (
                                <div className="invalid-feedback d-block">
                                    {errors.description.message}
                                </div>)}
                        </div>
                    </div>
                </div>
            </BaseForm>
        </form>
    );
}
export default Form;