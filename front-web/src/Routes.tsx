import { Router, Redirect, Route, Switch } from 'react-router-dom';
import Navbar from './core/components/navbar';
import Admin from './pages/Admin';
import Catalog from './pages/Catalog';
import ProductDetails from './pages/Catalog/components/ProductDatails';
import Home from './pages/Home';
import Auth from './pages/Auth';
import history from './core/utils/history';
import Access403 from 'pages/Auth/Acess403';


const Routes = () => (
    <Router history={history}>
        <Navbar />
        <Switch>
            <Route path="/" exact>
                <Home />
            </Route>
            <Route path="/403">
                <Access403 />
            </Route>
            <Route path="/products" exact>
                <Catalog />
            </Route>
            <Route path="/products/:productId">
                <ProductDetails />
            </Route>
            <Redirect from="/auth" to="auth/login" exact />
            <Route path="/auth">
                <Auth />
            </Route>
            <Redirect from="/admin" to="/admin/products" exact />
            <Route path="/admin">
                <Admin />
            </Route>
            <Route path="/">
                <Access403 />
            </Route>
        </Switch>
    </Router>

);

export default Routes;