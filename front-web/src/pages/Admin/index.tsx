import './styles.scss';
import Navbar from "./components/Navbar";
import { Switch } from 'react-router';
import Products from './components/Products';
import PrivateRoute from 'core/components/Routes/PrivateRoute';
import Categories from './components/Categories';
import Users from './components/Users';
import SubCategories from './components/SubCategories';
//import { isAuthenticated } from 'core/utils/auth';

const Admin = () => (
    <div className="admin-container">
        <Navbar/>
        <div className="admin-content">
            <Switch>
            
                <PrivateRoute path="/admin/products">
                    <Products/>
                </PrivateRoute>
                <PrivateRoute path="/admin/categories" >
                    <Categories/>
                </PrivateRoute>
                <PrivateRoute path="/admin/subcategories" >
                    <SubCategories/>
                </PrivateRoute>
                <PrivateRoute path="/admin/users" allowedRoutes={['ROLE_ADMIN']}>
                    <Users/>
                </PrivateRoute>
            </Switch>
        </div>
    </div>
);

export default Admin;