package com.devsuperior.dscatalog.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devsuperior.dscatalog.entities.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{
	
	//@Query(value="SELECT name FROM TB_CATEGORY WHERE LOWER(name) LIKE LOWER('%?1%') ", nativeQuery = true)
	Page<Category> findByNameContainingIgnoreCase(String name, Pageable pageable);
	
	

}
