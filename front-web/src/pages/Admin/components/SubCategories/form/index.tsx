import { toast } from 'react-toastify';
import { makePrivateRequest, makeRequest } from 'core/utils/request';
import { useForm, Controller } from 'react-hook-form';
import BaseForm from '../../BaseForm';
import './styles.scss';
import { useHistory, useParams } from 'react-router';
import { useEffect, useState } from 'react';
import Select from 'react-select';
import { Category, CategoryAdd, CategorySelect, SubCategories } from 'core/types/Product';



type ParamsType = {
    productId: string;
}
// const options = [
//     { value: 'livros', label: 'Livros' },
//     { value: 'eletronicos', label: 'Eletrônicos' },
//     { value: 'computadores', label: 'Computadores' }
// ]

const SubForm = () => {
    const { register, handleSubmit, errors, setValue, getValues, control } = useForm<SubCategories>();
    const history = useHistory();
    const [cat, setCAT] = useState<number>(3);
    const [isLoadingCategories, setIsloadingCategories] = useState(false);
    const [categories, setCategories] = useState<Category[]>([]);
    const { productId } = useParams<ParamsType>();
    const isEditing = productId !== 'create';
    const formTitle = isEditing ? "Editar Produto" : "Cadastrar um Produto";
    const [uploadImgUrl, setUploadImgUrl] = useState('');
    const [productImgUrl, setProductImgUrl] = useState('');
    // console.log(isEditing);
    // console.log(isEditing);


    useEffect(() => {
        setIsloadingCategories(true);
        makeRequest({ url: '/categories/all' })
            .then(response => setCategories(response.data))
            .finally((() => setIsloadingCategories(false)));
    }, []);
    
    const onSubmit = (data: SubCategories) => {
        console.log("data---->",data)
        
        console.log(getValues("category"),"getcalues")
        
        // makePrivateRequest({ url: '/subcategorie', method: 'POST', data })
        //     .then(() => {
        //         toast.info('SubCat cadastrado com sucesso!');
        //         //history.push('/admin/subcatagories');
        //     })
        //     .catch(() => {
        //         toast.error('Erro ao salvar produto!');
        //     })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <BaseForm title={formTitle}>
                <div className="product-form-container">
                    <div className="col-6">
                        <div className="margin-bottom-30">
                            <input name="name" type="text" placeholder="nome"
                                className="form-control input-base" ref={register({ required: "Campo obrigatório" })}></input>
                            {errors.name && (
                                <div className="invalid-feedback d-block">
                                    {errors.name.message}
                                </div>)}
                        </div>
                        <div className="margin-bottom-30">
                            <Controller defaultValue=""
                            name="category" rules={{required:true}} control={control} isLoading={isLoadingCategories}
                            as={Select} options={categories} getOptionLabel={(option: Category) => option.name}
                                getOptionValue={(option: Category) => {String(option.id); setCAT(option.id);}} 
                                placeholder="Categoria"  classNamePrefix="categories-select" onChange={(e:any)=>{setValue("category",e.id);console.log("teste",e)}}  />
                                {errors.category && (
                                <div className="invalid-feedback d-block">
                                    Campo obrigatório!!
                                </div>)}
                            {/* <Select className="filter-select-container" isClearable value={cat}
                                key={`select-${cat?.id}`} 
                                name="categories" isLoading={isLoadingCategories}
                                onChange={(e:any)=>{setValue("category",e.id);console.log(e); setCAT(e)}}
                                options={categories} getOptionLabel={(option: CategorySelect) => option.name}
                                getOptionValue={(option: CategorySelect) => String(option.id)}
                                placeholder="Categoria" classNamePrefix="product-categories-select" /> */}
                        </div>

                    </div>
                </div>
            </BaseForm>
        </form>
    );
}
export default SubForm;