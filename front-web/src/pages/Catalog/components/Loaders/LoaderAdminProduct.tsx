
import ContentLoader from "react-content-loader";
import { generateList } from "core/utils/list";

const LoaderAdminProduct = () => {
  const loaderItems = generateList(4);
  return (
    <>
      {loaderItems.map(item => (
       
        
       <div className="product-card-admin" key={item}>
        <ContentLoader
          speed={1}
          width={1000}
          height={135}
          viewBox="0 0 900 135"
          backgroundColor="#ecebeb"
          foregroundColor="#d6d2d2"

        >
          <rect x="0" y="0" rx="10" ry="10" width="800" height="135" />
        </ContentLoader>
        </div>
      ))}
    </>
  );
}
export default LoaderAdminProduct;