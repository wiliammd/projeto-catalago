package com.devsuperior.dscatalog.form;

import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.entities.SubCategorie;

public class SubCategorieForm {
	private Long id;
	private String name;
	private Long category;

	
	public SubCategorieForm(SubCategorie entity) {
		super();
		this.id = entity.getId();
		this.name = entity.getName();
		
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
