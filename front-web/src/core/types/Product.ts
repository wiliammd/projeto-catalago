export type ProductsResponse = {
    content:Product[];
    totalPages:number;
}
export type CategoriesResponse = {
    content: Category[];
    totalPages:number;
}
export type UsersResponse = {
    content: User[];
    totalPages:number;
}
export type User = {
    id:number;
    firstName:string;
    lastName:string;
    email:string;
    roles:Role[];
}
export type Role = {
    id:number;
    authority:string;
}

export type Product = {
    id: number;
    name: string;
    description: string;
    price:number;
    imgUrl:string;
    date:string;
    categories:Category[];
}
export type CategoryAdd = {
    id:number;
    name:string;
}
export type Category = {
    id:number;
    name:string;
    subCategories:SubCategories[];
}
export type CategorySelect ={
    id:number;
    name:string;
}
export type SubCategories ={
    id:number;
    name:string;
    category:Category
}

export type OrderBy = {
    id: number;
    name: string;
    orderBy: string;
    direction: string;
}
export type RoleForm ={
    id:number;
    authority:string;
    name:string;
}
export type ErroDto = {
    message: string;
    timestamp: string;
    status: number;
}