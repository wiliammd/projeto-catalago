import PrivateRoute from "core/components/Routes/PrivateRoute";
import { Route, Switch } from "react-router"
import Form from "./Form"


import List from "./List"

const Categories = () => {
    return (
        <div>
            <Switch>
                <Route path="/admin/categories" exact>
                    <List />
                </Route>
                <PrivateRoute path="/admin/categories/:categoryId" allowedRoutes={['ROLE_ADMIN']}>
                    <Form />
                </PrivateRoute>
            </Switch>
        </div>
    );
}
export default Categories;