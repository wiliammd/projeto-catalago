import PrivateRoute from "core/components/Routes/PrivateRoute";
import { Route, Switch } from "react-router"
import SubForm from "./form";
import ListSub from "./list";


const SubCategories = () => {
    return (
        <div>
            <Switch>
                <Route path="/admin/subcategories" exact>
                    <ListSub/>
                </Route>
                <PrivateRoute path="/admin/subcategories/create" allowedRoutes={['ROLE_ADMIN']}>
                   <SubForm/>
                </PrivateRoute>
            </Switch>
        </div>
    );
}
export default SubCategories;