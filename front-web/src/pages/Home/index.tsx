import './styles.scss';
import { ReactComponent as MainImage } from 'core/assets/images/main-image.svg'
import ButtonIcon from 'core/components/ButtonIcon';
import { Link } from 'react-router-dom'
import { useEffect } from 'react';
import { iniciarSessao, isTokenValid } from 'core/utils/auth';
const Home = () => {
    useEffect(() => {
        if(!isTokenValid()){
            iniciarSessao();
        }
    }, []);
    return (
    <div className="home-container">
        <div className="home-content card-base border-radius-20">
            <div className="home-text">
                <h1 className="text-title">
                    Conheça o melhor <br />catálogo de produtos
                </h1>
                <p className="text-subtitle">Ajudaremos você a encontrar os melhores<br /> produtos disponíveis no mercado.</p>
                <Link className="startSearchBtn" to="/products">
                    <ButtonIcon text="inicie agora sua busca" />
                </Link>
            </div>
            <div>
                <MainImage className="main-image" />
            </div>
        </div>
    </div>
);
}

export default Home;