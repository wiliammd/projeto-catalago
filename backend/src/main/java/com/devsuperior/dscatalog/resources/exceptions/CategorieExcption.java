package com.devsuperior.dscatalog.resources.exceptions;

public class CategorieExcption extends RuntimeException {
private static final long serialVersionUID = 1L;
	
	public CategorieExcption(String msg) {
		super(msg);
	}
}
