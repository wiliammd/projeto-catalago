import PrivateRoute from "core/components/Routes/PrivateRoute";
import { Route, Switch } from "react-router"
import Form from "./Form";
import List from "./List";


const Users = () => {
    return (
        <div>
            <Switch>
                <Route path="/admin/users" exact>
                    <List/>
                </Route>
                <PrivateRoute path="/admin/users/:userId" allowedRoutes={['ROLE_ADMIN']}>
                    <Form/>
                </PrivateRoute>
            </Switch>
        </div>
    );
}
export default Users;