import './styles.scss';
import AuthCard from '../Card';
import { Link, useHistory, useLocation } from 'react-router-dom';
import ButtonIcon from 'core/components/ButtonIcon';
import { useForm } from "react-hook-form";
import { makeLogin } from 'core/utils/request';
import { useState } from 'react';
import { saveSessionData } from 'core/utils/auth';

type FormState = {
    username: string;
    password: string;
}
type LocationState = {
    from:string;
}


const Login = () => {
    const { register, handleSubmit, errors } = useForm<FormState>();
    const [hasError, setHasError] = useState(false);
    
    const history = useHistory();
    const location = useLocation<LocationState>();

    const { from } = location.state || { from: { pathname: "/admin" } };

    const onSubmit = (data: FormState) => {
        makeLogin(data)
            .then(response => {
                setHasError(false);
                saveSessionData(response.data);
                history.replace(from);
            })
            .catch(() => { setHasError(true); })
    }

    return (
        <AuthCard title="Login">
            {hasError && (
                <div className="alert alert-danger mt-5 login-alerta">
                    Usuario ou Senha invalidos!
                </div>)}
            <form onSubmit={handleSubmit(onSubmit)} className="login-form">
                <div className="margin-bottom-30">
                    <input ref={register({
                        required: "Campo obrigatório",
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Email inválido"
                        }
                    })} type="email" name="username" className={`form-control input-base ${errors.username? 'is-invalid':''}`} 
                    placeholder="Email" />
                    {errors.username && (
                        <div className="invalid-feedback d-block">
                            {errors.username.message}
                        </div>)}
                </div>
                <div className="margin-bottom-30">
                    <input ref={register({ required: "Campo obrigatório", minLength:{value:5, message:"Senha deve ter mais que 5 caracteres"}  })} 
                    type="password" name="password" className={`form-control input-base ${errors.password? 'is-invalid':''}`}
                     placeholder="Password" />
                    {errors.password && (
                        <div className="invalid-feedback d-block">
                            {errors.password.message}
                        </div>)}
                </div>

                <Link to="/auth/recover" className="login-link-recover">esqueci a senha?
                </Link>
                <div className="login-submit">
                    <ButtonIcon text="Logar" />
                </div>
                <div className="text-center">
                    <span className="not-registred">Não tem cadastro?</span>
                    <Link to="/auth/register" className="login-link-register">
                        Cadastrar
                    </Link>
                </div>
            </form>
        </AuthCard>
    )
}
export default Login;