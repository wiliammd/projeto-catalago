package com.devsuperior.dscatalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devsuperior.dscatalog.entities.SubCategorie;
@Repository
public interface SubCategorieRepository extends JpaRepository<SubCategorie, Long> {

}
