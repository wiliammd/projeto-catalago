package com.devsuperior.dscatalog.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.devsuperior.dscatalog.dto.CategoryDTO;
import com.devsuperior.dscatalog.dto.SubCategorieDTO;
import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.entities.SubCategorie;
import com.devsuperior.dscatalog.repositories.CategoryRepository;
import com.devsuperior.dscatalog.repositories.SubCategorieRepository;

@Service
public class SubCategorieService {
	@Autowired
	private SubCategorieRepository subCatrepo;
	@Autowired
	private CategoryRepository catRepo;
	@Transactional(readOnly = true)
	public Page<SubCategorie> findAllPaged(PageRequest pageRequest){
		Page<SubCategorie>list =  subCatrepo.findAll(pageRequest);
		return list;
	}
	@Transactional
	public SubCategorieDTO insert(SubCategorieDTO form) {
		SubCategorie entity = new SubCategorie();
		//aqui no caso eu receberia o id e faria a busca porém o useForm meu esta na versao antiga de qnd comecei o curso
		//e hoje uso a mais atual onde permite usar o Controller com a propriedade render para anterar seValue antes do envio do form
		Optional<Category> cat = catRepo.findById(form.getCategory());
		entity.setName(form.getName());
		System.out.println("Cat->"+cat.get());
		
		entity.setCategory(cat.get());
		entity = subCatrepo.save(entity);
		return new SubCategorieDTO(entity);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity <CategoryDTO> delete(@PathVariable Long id){
		subCatrepo.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
